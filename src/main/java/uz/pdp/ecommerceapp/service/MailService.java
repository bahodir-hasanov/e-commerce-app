package uz.pdp.ecommerceapp.service;

import com.stripe.model.checkout.Session;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import uz.pdp.ecommerceapp.dto.CustomProduct;
import uz.pdp.ecommerceapp.model.AttachmentContent;
import uz.pdp.ecommerceapp.model.User;
import uz.pdp.ecommerceapp.projection.CustomProductForEmail;
import uz.pdp.ecommerceapp.repository.ProductRepository;
import uz.pdp.ecommerceapp.repository.UserRepository;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.*;


@RequiredArgsConstructor
@Service("mailService")
public class MailService {


    @Value("${PERSONAL}")
    String personal;

    private final JavaMailSender javaMailSender;

    private final TemplateEngine templateEngine;

    private final ProductRepository productRepository;

    private final UserRepository userRepository;


    public void sendEmailToClient(Session session, UUID userId) {

        List<CustomProductForEmail> products = productRepository.getProductsForEmailById(userId);
//        List<CustomProduct> productList = new ArrayList<>();
        double totalPrice = 0;
        for (CustomProductForEmail product : products) {
            totalPrice += product.getPrice();
            AttachmentContent photo = product.getPhoto();
            String url = Base64.getEncoder().encodeToString(photo.getData());
            CustomProduct customProduct = new CustomProduct(product.getId(), url, product.getName(), product.getPrice());
//            productList.add(customProduct);
        }
        User user = userRepository.findById(userId).get();
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(message,
                    MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());
            Context context = new Context();
            Map<String, Object> props = new HashMap<>();
            props.put("fullName", user.getFullName());
            props.put("products", products);
            props.put("totalPrice", totalPrice);
            context.setVariables(props);
            helper.setFrom("abdisattarovasevinch5@gmail.com");
            helper.setTo(session.getCustomerDetails().getEmail());
            helper.setSubject("Purchase info");
            String html = templateEngine.process("email.html", context);
            helper.setText(html, true);
//            products.stream().forEach(customProduct -> {
//                try {
//                    helper.addInline(customProduct.getName(), new ByteArrayResource(customProduct.getPhoto().getData()));
//                } catch (MessagingException e) {
//                    e.printStackTrace();
//                }
//            });

            javaMailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
