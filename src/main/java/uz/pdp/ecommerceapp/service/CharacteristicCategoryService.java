package uz.pdp.ecommerceapp.service;
//Sevinch Abdisattorova 04/11/2022 9:52 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.model.CharacteristicCategory;
import uz.pdp.ecommerceapp.projection.CustomCharacteristicCategory;
import uz.pdp.ecommerceapp.repository.CharacteristicCategoryRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class CharacteristicCategoryService {


    private final CharacteristicCategoryRepository characteristicCategoryRepository;


    public ResponseEntity<?> getAllCategories() {
        List<CustomCharacteristicCategory> all = characteristicCategoryRepository.getAllCategories();
        return all.size() > 0 ? ResponseEntity.ok(all) : ResponseEntity.ok("No categories yet!");
    }


    public ResponseEntity<?> addNewCategory(String name) {
        CharacteristicCategory byName = characteristicCategoryRepository.findByName(name);
        if (byName == null) {
            characteristicCategoryRepository.save(new CharacteristicCategory(name));
            return new ResponseEntity<>("Successfully saved!", HttpStatus.CREATED);
        }
        return new ResponseEntity<>("Category with this name already exists!", HttpStatus.CONFLICT);
    }


    public ResponseEntity<?> editCategoryById(String name, UUID id) {
        Optional<CharacteristicCategory> byId =
                characteristicCategoryRepository.findById(id);
        if (byId.isPresent()) {
            CharacteristicCategory characteristicCategory = byId.get();
            characteristicCategory.setName(name);
            characteristicCategoryRepository.save(characteristicCategory);
            return new ResponseEntity<>("Successfully updated!", HttpStatus.OK);
        }
        return new ResponseEntity<>("Category with this id not found!", HttpStatus.NOT_FOUND);
    }


    public ResponseEntity<?> deleteCategoryById(UUID id) {
        try {
            characteristicCategoryRepository.deleteById(id);
            return new ResponseEntity<>("Successfully deleted", HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("You can't delete this category!", HttpStatus.CONFLICT);
    }
}

