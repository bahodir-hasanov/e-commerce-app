package uz.pdp.ecommerceapp.service;//@AllArgsConstructor

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.dto.ProductRateDto;
import uz.pdp.ecommerceapp.model.FavoriteProduct;
import uz.pdp.ecommerceapp.model.Product;
import uz.pdp.ecommerceapp.model.ProductRates;
import uz.pdp.ecommerceapp.model.User;
import uz.pdp.ecommerceapp.repository.FavoriteProductRepository;
import uz.pdp.ecommerceapp.repository.ProductRateRepository;
import uz.pdp.ecommerceapp.repository.ProductRepository;
import uz.pdp.ecommerceapp.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

//@NoArgsConstructor
//@Data
//@Entity
//import static uz.sardor.Main.*;
//Sardor {4/12/2022}{ 4:02 AM}
@RequiredArgsConstructor
@Service
public class ProductRateService {
    private final ProductRateRepository productRateRepository;
    private final UserRepository userRepository;
    private final ProductRepository productRepository;

    public ResponseEntity<?> addProductRate(ProductRateDto productRateDto) {

        Optional<User> optionalUser = userRepository.findById(productRateDto.getUserId());
        Optional<Product> optionalProduct = productRepository.findById(productRateDto.getProductId());

        if (!optionalProduct.isPresent()) {
            return new ResponseEntity<>("Product not found", HttpStatus.BAD_REQUEST);
        }
        if (!optionalUser.isPresent()) {
            return new ResponseEntity<>("User not found", HttpStatus.BAD_REQUEST);
        }
        boolean existsByUserIdAndProductId = productRateRepository.existsByUserIdAndProductId(productRateDto.getUserId(), productRateDto.getProductId());

        if (existsByUserIdAndProductId) {
            return new ResponseEntity<>("This product is rated", HttpStatus.CONFLICT);
        }
        ProductRates productRate = new ProductRates(optionalUser.get(),optionalProduct.get(), productRateDto.getRate());
        productRateRepository.save(productRate);
        return new ResponseEntity<>("Successfully added", HttpStatus.ACCEPTED);
    }


    public ResponseEntity<?> deleteProductRate(UUID id) {
        Optional<ProductRates> productRate = productRateRepository.findById(id);
        if (!productRate.isPresent()) {
            return new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);
        }
        productRateRepository.deleteById(id);
        return new ResponseEntity<>("Successfully deleted", HttpStatus.ACCEPTED);
    }


    public ResponseEntity<?> updateProductRate(UUID id, short rate) {
        Optional<ProductRates> optionalProductRate = productRateRepository.findById(id);
        if (!optionalProductRate.isPresent()) {
            return new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);
        }
        optionalProductRate.get().setRate(rate);
        productRateRepository.save(optionalProductRate.get());
        return new ResponseEntity<>("Successfully updated", HttpStatus.ACCEPTED);
    }

    public ResponseEntity<?> getAllProductRateByProductId(UUID productId) {
        List<ProductRates> productRateRepositoryAllByProductId = productRateRepository.getAllByProductId(productId);
        if (productRateRepositoryAllByProductId.isEmpty()) {
            return new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(productRateRepositoryAllByProductId, HttpStatus.ACCEPTED);


    }

}
