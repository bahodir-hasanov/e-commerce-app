package uz.pdp.ecommerceapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.dto.AdminUserDto;
import uz.pdp.ecommerceapp.model.Role;
import uz.pdp.ecommerceapp.model.User;
import uz.pdp.ecommerceapp.payload.ApiResponse;
import uz.pdp.ecommerceapp.repository.RoleRepository;
import uz.pdp.ecommerceapp.repository.UserRepository;

import java.util.HashSet;
import java.util.Set;

// Bahodir Hasanov 4/14/2022 11:30 PM

@Service
public class AdminUserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    RoleRepository roleRepository;

    public ApiResponse saveNewUserByAdmin(AdminUserDto adminUserDto) {
        if (userRepository.existsByEmail(adminUserDto.getEmail())) {
            return new ApiResponse("this email already exist", false);
        }
        User user = new User();
        user.setFullName(adminUserDto.getFullName());
        user.setEmail(adminUserDto.getEmail());
        user.setPassword(passwordEncoder.encode(adminUserDto.getPassword()));
        user.setPhoneNumber(adminUserDto.getPhoneNumber());
        Set<Role> roleList = new HashSet<>() ;
        adminUserDto.getRolesId().forEach(uuid ->roleList.add(roleRepository.getById(uuid)));
        user.setRoles(roleList);
        user.setEnabled(true);
        userRepository.save(user);
        return new ApiResponse("successfully saved",true);

    }
}
