package uz.pdp.ecommerceapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.ecommerceapp.dto.RegisterDto;
import uz.pdp.ecommerceapp.model.User;
import uz.pdp.ecommerceapp.model.enums.RoleEnum;
import uz.pdp.ecommerceapp.payload.ApiResponse;
import uz.pdp.ecommerceapp.repository.RoleRepository;
import uz.pdp.ecommerceapp.repository.UserRepository;


// Bahodir Hasanov 4/14/2022 3:09 PM

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    public ApiResponse registerUser(RegisterDto registerDto) {
        if(!registerDto.getPassword().equals(registerDto.getPrePassword())){
            return new ApiResponse("passwords not equals", false);
        }
        boolean byEmail = userRepository.existsByEmail(registerDto.getEmail());
        if (byEmail) {
            return new ApiResponse("this email already exist", false);
        }
        User user = new User();
        user.setFullName(registerDto.getFullName());
        user.setEmail(registerDto.getEmail());
        user.setPassword(passwordEncoder.encode(registerDto.getPassword()));
        user.setPhoneNumber(registerDto.getPhoneNumber());
        user.setRoles( roleRepository.findByRole(RoleEnum.ROLE_USER));
        user.setEnabled(true);
        userRepository.save(user);
        return new ApiResponse("successfully registered",true);
    }

    public UserDetails loadUserByUsername(String phoneNumber) {
        User byPhoneNumber = userRepository.findByPhoneNumber(phoneNumber);
        return byPhoneNumber;
    }
}
