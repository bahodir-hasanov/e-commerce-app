package uz.pdp.ecommerceapp.model;
//Sevinch Abdisattorova 04/11/2022 2:06 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.ecommerceapp.model.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity(name = "product_rates")
public class ProductRates extends AbsEntity {

    @ManyToOne
    User user;

    @ManyToOne
    Product product;

    Short rate;





}
