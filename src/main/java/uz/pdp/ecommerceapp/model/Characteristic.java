package uz.pdp.ecommerceapp.model;
//Sevinch Abdisattorova 04/11/2022 2:17 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import uz.pdp.ecommerceapp.model.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity(name = "characteristics")
public class Characteristic extends AbsEntity {

    String name;

    @Column(columnDefinition = "boolean default false")
    Boolean isMain;

    @ManyToOne
    CharacteristicCategory characteristicCategory;

}
