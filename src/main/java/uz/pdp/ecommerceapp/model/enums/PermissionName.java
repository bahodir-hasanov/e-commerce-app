package uz.pdp.ecommerceapp.model.enums;

import javax.persistence.Enumerated;

public enum PermissionName {
    /* Role permission */

    ADD_ROLE,
    EDIT_ROLE,
    DELETE_ROLE,
    GET_ROLES,


    /* Product permissions */

    ADD_PRODUCT,
    EDIT_PRODUCT,
    DELETE_PRODUCT,
    GET_PRODUCT,
    GET_PRODUCT_BY_ID,
    GET_PRODUCT_CHARACTERISTIC_BY_ID,

    /* User permissions */

    ADD_USER,
    EDIT_USER,
    DELETE_USER,
    GET_USERS,

    /* Characteristics permissions*/

    ADD_CHARACTERISTIC,
    GET_CHARACTERISTICS,
    EDIT_CHARACTERISTIC,
    DELETE_CHARACTERISTIC,


    /* Characteristics category permissions*/

    ADD_CHARACTERISTIC_CATEGORY,
    GET_CHARACTERISTICS_CATEGORIES,
    EDIT_CHARACTERISTIC_CATEGORY,
    DELETE_CHARACTERISTIC_CATEGORY,


    /*Characteristic value permissions*/
    GET_CHARACTERISTIC_VALUES,
    DELETE_CHARACTERISTIC_VALUE,


    /*Cart permissions*/
    GET_CART_ITEMS,
    ADD_PRODUCT_TO_CART,
    DELETE_PRODUCT_FROM_CART,
    CLEAR_CART,

    /*Order permissions*/
    GET_USER_ORDERS,
    GET_ALL_NEW_ORDERS,
    GET_ALL_ORDERS,
    GET_ALL_ORDER_PRODUCTS_OF_ORDER,

    /* Admin dashboard permission*/
    PRODUCTS_COUNT_BY_CATEGORY,
    PRODUCTS_OF_CATEGORY,
    GET_MOST_FAVOURITE_PRODUCTS,
    GET_USERS_LIKED_PRODUCT,
    COUNT_USERS_BY_ROLE,
    GET_USERS_BY_ROLE


}
