package uz.pdp.ecommerceapp.repository;
//Sevinch Abdisattorova 04/11/2022 9:50 PM

//Sevinch Abdisattorova 04/11/2022 9:50 PM

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.ecommerceapp.model.Category;
import uz.pdp.ecommerceapp.model.PayType;

import java.util.List;
import java.util.UUID;

@Repository
public interface CategoryRepository extends JpaRepository<Category, UUID> {

    boolean findByName(String string);

    @Query(nativeQuery = true, value = "select *\n" +
            "from categories\n" +
            "where categories.parent_category_id IS NULL")
    List<Category> getMainCategory();

    List<Category> getCategoryByParentCategoryId(UUID parentCategory_id);
}
