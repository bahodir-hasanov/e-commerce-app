package uz.pdp.ecommerceapp.repository;
//Sevinch Abdisattorova 04/11/2022 9:50 PM


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.ecommerceapp.model.Characteristic;
import uz.pdp.ecommerceapp.model.Value;
import uz.pdp.ecommerceapp.projection.CustomCharacteristic;

import java.util.List;
import java.util.UUID;

@Repository
public interface ValueRepository extends JpaRepository<Value, UUID> {

    Value findByValue(String value);
}
