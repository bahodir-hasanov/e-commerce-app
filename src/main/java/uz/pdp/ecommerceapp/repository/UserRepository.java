package uz.pdp.ecommerceapp.repository;
//Sevinch Abdisattorova 04/11/2022 9:50 PM


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.ecommerceapp.model.User;
import uz.pdp.ecommerceapp.projection.CustomUser;
import uz.pdp.ecommerceapp.projection.CustomUserStatsByRole;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

    User findByEmail(String email);

    User findByPhoneNumber(String phoneNumber);

    boolean existsByEmail(String email);


    @Query(nativeQuery = true, value = "select count(*)\n" +
            "from users\n" +
            "where created_at::date = :dateTime ;\n ")
    Integer getUsersCountRegisteredInDate(String dateTime);


    @Query(nativeQuery = true, value = "select count(*)\n" +
            "from users\n" +
            "where extract(month from created_at) = :month\n" +
            "  and extract(year from created_at) = :year\n")
    Integer getUsersCountRegisteredInMonth(Integer month, Integer year);


    @Query(nativeQuery = true, value = "select count(*)\n" +
            "from users\n" +
            "where extract(year from created_at) = :year\n")
    Integer getUsersCountRegisteredInYear(Integer year);

    @Query(nativeQuery = true, value = "select count(*) " +
            " from users")
    Integer countAllUsers();


    @Query(nativeQuery = true, value = "select distinct cast(u.id as varchar) as id,\n" +
            "                u.full_name           as fullName\n" +
            "from users u\n" +
            "         join favorites_products fp on u.id = fp.user_id\n" +
            "where product_id = :productId and" +
            " u.full_name ilike concat('%',:search,'%')")
    Page<CustomUser> getUsersLikedThisProduct(Pageable pageable, UUID productId, String search);


    @Query(nativeQuery = true, value = "select r.role   as role,\n" +
            "       count(*) as count\n" +
            "from users\n" +
            "         join users_roles ur on users.id = ur.user_id\n" +
            "         join roles r on r.id = ur.role_id\n" +
            "group by r.role")
    List<CustomUserStatsByRole> countUsersByRole();


    @Query(nativeQuery = true, value = "select distinct cast(u.id as varchar) as id,\n" +
            "                u.full_name           as fullName\n" +
            "from users u\n" +
            "         join users_roles ur on u.id = ur.user_id\n" +
            "         join roles r on r.id = ur.role_id\n" +
            "where r.id = :roleId\n" +
            "  and u.full_name ilike concat('%', :search, '%')")
    Page<CustomUser> getUsersByRole(Pageable pageable, UUID roleId, String search);


}
