package uz.pdp.ecommerceapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.ecommerceapp.model.AdditionalInMonth;


import java.util.UUID;


public interface AdditionalInMonthRepository extends JpaRepository<AdditionalInMonth, UUID> {

}
