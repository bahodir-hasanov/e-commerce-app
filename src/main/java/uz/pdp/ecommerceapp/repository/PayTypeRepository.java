package uz.pdp.ecommerceapp.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.ecommerceapp.model.PayType;

import java.util.UUID;

@Repository
public interface PayTypeRepository extends JpaRepository<PayType, UUID> {

    PayType findByName(String name);

}
