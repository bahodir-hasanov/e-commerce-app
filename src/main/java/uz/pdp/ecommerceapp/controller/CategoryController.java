package uz.pdp.ecommerceapp.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommerceapp.model.Category;
import uz.pdp.ecommerceapp.service.CategoryService;

import java.util.UUID;


@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/category")
public class CategoryController {

    public final CategoryService categoryService;


    @PostMapping
    public ResponseEntity<?> addNewCategory(@RequestBody Category category) {
        return categoryService.addCategory(category);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> showCategoryById(@PathVariable UUID id) {
        return categoryService.getCategoryById(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable UUID id) {

        return categoryService.deleteCategoryById(id);
    }

    @GetMapping
    public ResponseEntity<?> showMainCategories() {
        return categoryService.getMainCategories();
    }

    @GetMapping("/parent/{parentCategoryId}")
    public ResponseEntity<?> showCategoriesByParentCategoryId(@PathVariable UUID parentCategoryId) {
        return categoryService.getCategoriesByParentCategoryId(parentCategoryId);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateCategory(@PathVariable UUID id, @RequestBody Category category) {
        return categoryService.updateCategoryById(category, id);
    }


}
