package uz.pdp.ecommerceapp.controller;
//Sevinch Abdisattorova 04/11/2022 9:46 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommerceapp.service.CharacteristicCategoryService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/characteristics-category")
public class CharacteristicCategoryController {

    private final CharacteristicCategoryService characteristicCategoryService;


    @PreAuthorize(value = "hasAuthority('GET_CHARACTERISTICS_CATEGORIES')")
    @GetMapping
    public ResponseEntity<?> showAllCategories() {
        return characteristicCategoryService.getAllCategories();
    }



    @PreAuthorize(value = "hasAuthority('ADD_CHARACTERISTIC_CATEGORY')")
    @PostMapping
    public ResponseEntity<?> addNewCategory(@RequestParam String name) {
        return characteristicCategoryService.addNewCategory(name);
    }



    @PreAuthorize(value = "hasAuthority('EDIT_CHARACTERISTIC_CATEGORY')")
    @PutMapping("/{id}")
    public ResponseEntity<?> editCategory(@RequestParam String name, @PathVariable UUID id) {
        return characteristicCategoryService.editCategoryById(name, id);
    }



    @PreAuthorize(value = "hasAuthority('DELETE_CHARACTERISTIC_CATEGORY')")
    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteCategory(@PathVariable UUID id) {
        return characteristicCategoryService.deleteCategoryById(id);
    }
}
