package uz.pdp.ecommerceapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.ecommerceapp.dto.AdminUserDto;
import uz.pdp.ecommerceapp.payload.ApiResponse;
import uz.pdp.ecommerceapp.service.AdminUserService;

// Bahodir Hasanov 4/14/2022 11:28 PM
@RestController
public class AdminUserController {

    @Autowired
    AdminUserService adminUserService;

    public ResponseEntity<?> saveNewUserByAdmin(@RequestBody AdminUserDto adminUserDto) {
        ApiResponse apiResponse = adminUserService.saveNewUserByAdmin(adminUserDto);
        return ResponseEntity.status(apiResponse.isStatus()?200:409).body(apiResponse);
    }
}
