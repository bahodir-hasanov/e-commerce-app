package uz.pdp.ecommerceapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.ecommerceapp.dto.ProductDto;
import uz.pdp.ecommerceapp.service.ProductService;

import java.util.List;
import java.util.UUID;

import static uz.pdp.ecommerceapp.utils.Constants.DEFAULT_PAGE_SIZE;

// Bahodir Hasanov 4/11/2022 9:01 PM
@RestController
@RequestMapping("/api/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping
    @PreAuthorize(value = "hasAuthority('GET_PRODUCT')")
    public ResponseEntity<?> getAllProduct(
            @RequestParam(name = "size", defaultValue = DEFAULT_PAGE_SIZE) int size,
            @RequestParam(name = "page", defaultValue = "1") int page,
            @RequestParam(name = "search", defaultValue = "") String search,
            @RequestParam(name = "sort", defaultValue = "name") String sort
    ) {
        return productService.getAllProducts(page, size, search, sort, true);
    }

    @PostMapping
    public ResponseEntity<?> saveProduct(
            @RequestPart List<MultipartFile> files,
            @RequestPart(name = "product") ProductDto productDto) {
        return productService.saveProduct(files, productDto);
    }

    @GetMapping("/{productId}")
    public ResponseEntity<?> getProductById(@PathVariable UUID productId) {
        return productService.getProductById(productId);
    }

    @GetMapping("/characteristics/{productId}")
    public ResponseEntity<?> getProductWithCharacteristics(@PathVariable UUID productId) {
        return productService.getProductWithCharacteristics(productId);
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<?> deleteProductById(@PathVariable UUID productId){
        return productService.deleteProductById(productId);
    }

}
