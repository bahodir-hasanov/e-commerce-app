package uz.pdp.ecommerceapp.controller;
//Sevinch Abdisattorova 04/13/2022 04:34 AM

import com.stripe.Stripe;
import com.stripe.model.Event;
import com.stripe.model.StripeObject;
import com.stripe.model.checkout.Session;
import com.stripe.net.Webhook;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommerceapp.model.Order;
import uz.pdp.ecommerceapp.model.enums.Status;
import uz.pdp.ecommerceapp.repository.OrderRepository;
import uz.pdp.ecommerceapp.service.MailService;
import uz.pdp.ecommerceapp.service.OrderService;
import uz.pdp.ecommerceapp.service.TransactionHistoryService;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/stripe-webhook")
@RequiredArgsConstructor
public class StripeEventController {


    @Value("${STRIPE_SECRET_KEY}")
    String stripeApiKey;

    @Value("${WEBHOOK_SECRET_KEY}")
    String webhookSecretKey;


    private final OrderRepository orderRepository;
    private final OrderService orderService;
    private final TransactionHistoryService transactionHistoryService;
    private final MailService mailService;

    @PostMapping
    public void handle(@RequestBody String payload,
                       @RequestHeader(name = "Stripe-Signature") String sigHeader) {
        Stripe.apiKey = stripeApiKey;
        String endpointSecret = webhookSecretKey;
        Event event = null;

        try {
            event = Webhook.constructEvent(payload, sigHeader, endpointSecret);
            System.out.println("Event type: " + event.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }


        assert event != null;
        if ("checkout.session.completed".equals(event.getType())) {
            Optional<StripeObject> object = event.getDataObjectDeserializer().getObject();
            if (object.isPresent()) {
                Session session = (Session) object.get();
                fulfillOrder(session);
            }
            System.out.println("Got payload: " + payload);
        }

    }


    public void fulfillOrder(Session session) {

        String clientReferenceId = session.getClientReferenceId();
        UUID userId = UUID.fromString(clientReferenceId);

        String paymentIntent = session.getPaymentIntent();
        double amountTotal = session.getAmountTotal().doubleValue() / 100;
        Order order = orderRepository.findByUserIdAndStatus(userId, Status.NEW);

        // TODO: 04/13/2022 save transaction history / send email to client
        transactionHistoryService.saveTransactionHistory(order, amountTotal, paymentIntent);
        mailService.sendEmailToClient(session, userId);
        orderService.changeOrderStatusToPurchased(order);
    }
}
