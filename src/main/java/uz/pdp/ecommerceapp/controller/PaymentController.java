package uz.pdp.ecommerceapp.controller;
//Sevinch Abdisattorova 04/13/2022 04:34 AM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommerceapp.dto.UserDto;
import uz.pdp.ecommerceapp.service.PaymentService;
import uz.pdp.ecommerceapp.service.UserService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/purchase")
public class PaymentController {


    private final PaymentService paymentService;
    private final UserService userService;


    @GetMapping
    public ResponseEntity<?> purchaseProducts(HttpServletRequest request,
                                              HttpServletResponse response) {

        Cookie cookie1 = Arrays.stream(request.getCookies()).
                filter(cookie -> cookie.getName()
                        .equals("user"))
                .findFirst()
                .orElse(null);

        if (cookie1 != null) {
            return paymentService.purchaseProductsInTheCart(request, response);
        } else {
            return ResponseEntity.ok("=> Enter your info (fullName,phoneNumber): /api/purchase [post] ");
        }
    }


    @PostMapping
    public ResponseEntity<?> authenticateForPurchase(@RequestBody UserDto userDto,
                                                     HttpServletResponse response) {
        return userService.saveCurrentUser(userDto, response);
    }


    @PostMapping("/{code}")
    public ResponseEntity<?> confirmPassword(@PathVariable Integer code,
                                             HttpServletResponse response,
                                             HttpServletRequest request) {

        return paymentService.confirmPassword(code, response, request);
    }
}
