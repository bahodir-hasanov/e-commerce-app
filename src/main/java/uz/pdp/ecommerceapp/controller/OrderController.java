package uz.pdp.ecommerceapp.controller;


//Sevinch Abdisattorova 04/13/2022 06:34 AM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommerceapp.service.OrderService;


@RestController
@RequestMapping("/api/order")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;


    @PreAuthorize(value = "hasAuthority('GET_ALL_ORDERS')")
    @GetMapping("/all")
    public ResponseEntity<?> getAllOrders(
    ) {
        return orderService.getAllOrders();
    }


    @PreAuthorize(value = "hasAuthority('GET_ALL_NEW_ORDERS')")
    @GetMapping("/new")
    public ResponseEntity<?> getAllNewOrders(
    ) {
        return orderService.getAllNewOrders();
    }


    @PreAuthorize(value = "hasAuthority('GET_USER_ORDERS')")
    @GetMapping
    public ResponseEntity<?> getOrdersOfUser(
    ) {
        return orderService.getOrdersOfUser();
    }
}
