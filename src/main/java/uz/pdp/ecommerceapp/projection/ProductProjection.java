package uz.pdp.ecommerceapp.projection;
// Bahodir Hasanov 4/11/2022 9:44 PM

import org.springframework.beans.factory.annotation.Value;

import java.util.UUID;

public interface ProductProjection {

    UUID getProductId();

    String getProductName();

    String getCategoryName();

    Double getPrice();

    @Value("#{@attachmentRepository.getOnlyOneAttachmentByProductId(target.productId)}")
    UUID getPhotoId();

}
