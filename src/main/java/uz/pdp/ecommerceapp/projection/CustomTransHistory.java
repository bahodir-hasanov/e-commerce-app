package uz.pdp.ecommerceapp.projection;

import org.springframework.beans.factory.annotation.Value;

import java.util.List;
import java.util.UUID;

public interface CustomTransHistory {

    UUID getUserId();

    String getUserFullName();

    UUID getOrderId();

    Integer getOrderSerialNumber();

    UUID getPayTypeId();

    String getPayTypeName();

}
